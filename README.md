# RISCV_VM

The VM is intended for anyone wanting to study, configure as-preferred, modify, physically-implement or sell hardware based the RISC-V Instruction Set Architecture. It includes: RV32M1 SW tools, RI5CY OVPSim model & ZERO_RISCY Verilator model

## Planned updates June 2020

 - VM OS patching and update to latest
 - tools patching and updating
 - CI / CD setup

